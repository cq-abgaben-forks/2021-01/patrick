#  +++++++++++++++++++++++++ Report: *Resistance Screening* +++++++++++++++++++++++++ 

## Resistance screening for: **patient1**
We identified potential resistance genes agains the following classes of antibiotcs:

| Aminoglycoside | Betalactame | Macrolide | Sulfonamide | Trimethoprime | 

 Recommendated antibiotic for treatment : Doxycyline (class: Tetracyclines)
------------------------------------------------------------------------------------------ 

## Resistance screening for: **patient2**
We identified potential resistance genes agains the following classes of antibiotcs:

| Aminoglycoside | Betalactame | Glycopeptide | Macrolide | Sulfonamide | Tetracycline | Trimethoprime | 

 Recommendated antibiotic for treatment : Chloramphenicol (class: Phenicols)
------------------------------------------------------------------------------------------ 

## Resistance screening for: **patient3**
We identified potential resistance genes agains the following classes of antibiotcs:

| Betalactame | Glycopeptide | Macrolide | Phenicol | Tetracycline | 

 Recommendated antibiotic for treatment : Gentamycine (class: Aminoglycosides)
------------------------------------------------------------------------------------------ 

# Resistance genes and corresponding antibiotcs classes
Abbreviation | antibiotic class
-------------|-------------
APH, StrA, StrB, SAT | Aminoglycosides
AmpC, TEM, OXA* | Betalactames
Dfr* | Trimethoprimes
Sul* | Sulfonamides
Mph* | Macrolides
Tet* | Tetracyclines
ANT* | Glycopeptides
Cat_Phe | Phenicol
