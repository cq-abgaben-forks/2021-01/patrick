nextflow.enable.dsl = 2

// Workflow for implementing sequence trimming, quality control, and identification of 
// causative mutations that mediate antibiotic resistance 
// aiming to give recommandation of antibiotic usage

//dafault cwd (where python script is saved)
params.cwd = baseDir // Assuming the python script is in the same directory as the nf file, see https://www.nextflow.io/docs/latest/script.html#implicit-variables
params.reference = "${baseDir}/CARD_v3.0.8_SRST2.fasta"  // Schoener, als hardgecoded
params.atblist = "${baseDir}/atblist.txt"

//default input directory:
params.indir = "./rawdata"


process fastp{
  storeDir "${params.outdir}/fastp_results/" 
  container "https://depot.galaxyproject.org/singularity/fastp%3A0.22.0--h2e03b76_0"
  input:
    path unprocessed_fastq
  output:
    path "${unprocessed_fastq.getSimpleName()}_trim.fastq", emit: fastq_trimmed
    path "${unprocessed_fastq.getSimpleName()}_fastp.json", emit: fastp_json
    path "${unprocessed_fastq.getSimpleName()}_fastp.html", emit: fastp_html
  script:
    """
    fastp -i ${unprocessed_fastq} -o ${unprocessed_fastq.getSimpleName()}_trim.fastq
    mv fastp.json ${unprocessed_fastq.getSimpleName()}_fastp.json
    mv fastp.html ${unprocessed_fastq.getSimpleName()}_fastp.html
    
    """
}

process fastqc{
  storeDir "${params.outdir}/quality_control/" 
  container "https://depot.galaxyproject.org/singularity/fastqc:0.11.9--0"
  input: 
    path fastq_file
  output:
    path "${fastq_file.getSimpleName()}_fastqc.html", emit: html_files
    path "${fastq_file.getSimpleName()}_fastqc.zip", emit: zip_files
  script:
    """
    fastqc ${fastq_file} 
    
    """
}

process multiqc {
  publishDir "${params.outdir}/quality_control/", mode: "copy", overwrite:  true 
  container "https://depot.galaxyproject.org/singularity/multiqc%3A1.9--pyh9f0ad1d_0"
  input:
    path input_folder
  output:
    path "*_report.html", emit: report
  script:
    """
    multiqc .
    """
}

process srst2{
  storeDir "${params.outdir}/srst2_analysis/"
  container "https://depot.galaxyproject.org/singularity/srst2%3A0.2.0--py27_2"
  input: 
    path inchannel
    
  output:
    path "${inchannel[0].getSimpleName()}__genes__*__results.txt"
  script:
    """
    srst2 --input_se ${inchannel[0]} --gene_db ${inchannel[1]} --output ${inchannel[0].getSimpleName()}
    """
}

process summary{
  publishDir "${params.outdir}", mode: "copy", overwrite:  true 
  input:
    path srst2_files
  output:
    path "srst2_results.txt"
  script:
    """
    cat ${srst2_files} > srst2_results.txt
    """

}

process recommendation{
  publishDir "${params.outdir}", mode: "copy", overwrite:  true 
  input:
    path summary_file 
  output:
    path "Empfehlungen.md"
  script:
    """
    python3 ${params.cwd}/recommendation.py ${summary_file} ${params.atblist} > Empfehlungen.md
    """

}
workflow res_pred {
    take:
        outdir
    
    main:
        
        params.outdir = outdir
        
        raw_fastq = channel.fromPath("${params.indir}/*")
        // testing strucuture of channel: raw_fastq.view()
        // --> no array, no flatten() required
        
        // sequence trimming with fastp:
        fastp_out = fastp(raw_fastq)
        
        // quality control with fastqc (of trimmed fastq-files)
        fastqc_out = fastqc(fastp_out.fastq_trimmed)

        // mulitqc summary of fastp trimming and fastqc analysis 
        // summary file is found: ./out/quality_control/multiqc.html
        inchannel_multiqc = fastqc_out.zip_files.concat(fastp_out.fastp_json)
        quality_report = multiqc(inchannel_multiqc.collect())


        // default srst2 reference:
        params.ref_file = params.reference // Schoener, als hardzucoden
        reference = channel.fromPath(params.ref_file)
        
        // resistance gene identification with srst2
        patient_fastas = fastp_out.fastq_trimmed.flatten()
        inchannel_srst2 = patient_fastas.combine(reference)

        srst2_output = srst2(inchannel_srst2)

        //combining srst2_outputs in one result file
        sum_file = summary(srst2_output.collect())
        
        //extra: python script reading the summary report and generating 
        //recommendation report
        
        report= recommendation(sum_file)
    
    emit:
        sum_file
        report
}


workflow {
    testrun = res_pred("./out")
    testrun.sum_file.view()
    testrun.report.view()
}


