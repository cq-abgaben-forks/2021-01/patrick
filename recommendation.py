# script to parse and interprete srst2 summary file

import sys 

summary_file = sys.argv[1]
atblistfile = sys.argv[2]

class Antibiotic:
    def __init__(self, name, classname):
        self.name = name
        self.classname = classname

    def __str__(self):
        return self.name + " (class: " + self.classname + ")"

resistance_dictionary = {"APH":"Aminoglycoside",
                         "STR":"Aminoglycoside",
                         "SAT":"Aminoglycoside",
                         "AMP":"Betalactame",
                         "TEM":"Betalactame",
                         "OXA":"Betalactame",
                         "DFR":"Trimethoprime",
                         "SUL":"Sulfonamide",
                         "MPH":"Macrolide",
                         "TET":"Tetracycline",
                         "ANT":"Glycopeptide",
                         "CAT":"Phenicol",
                         }

# Im Krankenhaus vorhandene Antibiotika aus Konfigurationsdatei einlesen
f = open(atblistfile, "r")
atb_order = [Antibiotic(x.split(",")[0].strip(), x.split(",")[1].strip()) for x in f.read().split("\n") if len(x.strip()) != 0]
f.close()


print("# ",25*"+", "Report: *Resistance Screening*", 25*"+", "\n")

with open (summary_file, "r") as f:
    data = f.readlines()
    for i in range(0, len(data), 2):
        patient = data[i+1].split()[0].split("_")[0]
        print("## Resistance screening for: **" + patient + "**")
        resistance_list = []
        detected_res_genes = data[i].split()
        for j in range(1, len(detected_res_genes)):
            current_key = detected_res_genes[j][:3].upper()
            if current_key in resistance_dictionary:
                if resistance_dictionary[current_key] not in resistance_list:
                    resistance_list.append(resistance_dictionary[current_key])
       
        # Daten aus der Datei nutzen, statt hart codierter Reihenfolge
        for atb in atb_order:
            if atb.classname not in resistance_list:
                recommendation = str(atb)
                break

        if "Tetracycline" not in resistance_list:
            recommendation = "Doxycyline (class: Tetracyclines)"
        elif "Aminoglycoside" not in resistance_list:
            recommendation = "Gentamycine (class: Aminoglycosides)"
        elif "Phenicol" not in resistance_list:
            recommendation = "Chloramphenicol (class: Phenicols)"
        else:
            recommendation = "Trimetoprim (class Diaminopyrimidines)"
        

        print("We identified potential resistance genes agains the following classes of antibiotcs:\n")
        
        resistance_genes_found = "| "
        sorted_list = sorted(resistance_list)
        for element in sorted_list:
            resistance_genes_found += element+" | "
        print(resistance_genes_found)
        
        
        print("\n Recommendated antibiotic for treatment : " + recommendation)
        print("-"*90, "\n")

print("# Resistance genes and corresponding antibiotcs classes")
print("Abbreviation | antibiotic class")
print("-------------|-------------")
print("APH, StrA, StrB, SAT | Aminoglycosides")
print("AmpC, TEM, OXA* | Betalactames")
print("Dfr* | Trimethoprimes")
print("Sul* | Sulfonamides")
print("Mph* | Macrolides")
print("Tet* | Tetracyclines")
print("ANT* | Glycopeptides")
print("Cat_Phe | Phenicol")



